import React from 'react';
import { connect } from 'react-redux';
import { Routes, Route } from 'react-router-dom';
import Header from './components/header';
import LoginPage from './pages/login.page';
import ActivityPage from './pages/activity.page';
import NewDebtorPage from './pages/new-debtor.page';
import ProfilePage from './pages/profile.page';
import ReportPage from './pages/report.page';
import SearchPage from './pages/search.page';
import WorkListPage from './pages/work-list.page';
import ActivityDebtorPage from "./pages/activities/activity-debtor.page"
type Props = {} & any;

type State = {};

class App extends React.Component<Props, State> {

    constructor(props: any) {
        super(props);

        this.state = {
            currentUser: undefined
        };
    }

    // componentDidMount() {
    //     console.log('Start', this.props)
    // }

    // componentWillUnmount() {
    //     console.log('unmount')
    // }

    render(): React.ReactNode {
        console.log('App: ', this.props)
        if (!this.props.authorization.user) {
            return (
                <>
                    <Header />
                    <Routes>
                        <Route path="/" element={<SearchPage />} />
                        <Route path="/new-debtor" element={<NewDebtorPage />} />
                        <Route path="/work-list" element={<WorkListPage />} />
                        <Route path="/search" element={<SearchPage />} />
                        <Route path="/profile" element={<ProfilePage />} />
                        <Route path="/activity/*" element={<ActivityPage />} />
                        <Route path="/report/*" element={<ReportPage />} />
                    </Routes>
                </>
            );
        }
        return (<LoginPage />);
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(App);