import React from 'react';

type Props = {};

type State = {};

class NewDebtor extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div>
                <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "50px" }}>บันทึกลูกหนี้</h5>
                <form className="row">
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ชื่อลูกหนี้</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">OC CODE</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ลูกหนี้ของสาขา/ฝ่าย</label>
                            <div className="col-sm-8">
                                <select className="form-select" >
                                    <option>Open this select menu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วันที่ซื้อคืน</label>
                            <div className="col-sm-8">
                                <input type="date" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">การได้มาซึ่งกรรมสิทธิ์</label>
                            <div className="col-sm-8">
                                <select className="form-select" >
                                    <option>Open this select menu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วงเงินกู้ (บาท)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ประเภทหนี้</label>
                            <div className="col-sm-8">
                                <select className="form-select" >
                                    <option>Open this select menu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เงินต้น (บาท)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ประเภทสินเชื่อ</label>
                            <div className="col-sm-8">
                                <select className="form-select" >
                                    <option>Open this select menu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ดอกเบี้ยรับ1 (บาท)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เลขที่บัญชี</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ดอกเบี้ยรับ2 (บาท)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 offset-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ดอกเบี้ยรับ2 (บาท)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row" style={{ width: "100%", maxWidth: "320px", float: "right" }}>
                            <div className="col-6">
                                <button type="reset" className="btn btn-primary w-100">บันทึก</button>
                            </div>
                            <div className="col-6">
                                <button type="submit" className="btn btn-secondary w-100">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default NewDebtor;