import React from 'react';
import { Link } from 'react-router-dom';

type Props = {};

type State = {};

class Header extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <nav className="navbar navbar-expand navbar-dark bg-primary-darker fixed-top" style={{ zIndex: 1 }}>
                <div className="container-fluid">
                    <Link className="navbar-brand" to="/" style={{ padding: "5px 0" }}>
                        <div style={{ marginLeft: "80px" }}>
                            <span style={{ marginBottom: '0', color: "#feac00", fontSize: "20px", fontWeight: "bold", lineHeight: "27px", letterSpacing: "2px" }}>AMIS</span>
                        </div>
                    </Link>
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav me-auto" style={{ borderLeft: "3px solid white", paddingLeft: "15px" }}>
                            <li className="nav-item">
                                <Link to="/new-debtor" className="nav-link">NEW DEBTOR</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/work-list" className="nav-link">WORKLISTS</Link>
                            </li>
                            {/* <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="#" onClick={e => e.preventDefault()}>
                                    Dropdown
                                </a>
                                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a className="dropdown-item" href="#">Action</a></li>
                                    <li><a className="dropdown-item" href="#">Another action</a></li>
                                    <li><hr className="dropdown-divider" /></li>
                                    <li><a className="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li> */}
                            <li className="nav-item">
                                <Link to="/report" className="nav-link">REPORTS</Link>
                            </li>
                            {/* <li className="nav-item">
                                <Link to="/activity" className="nav-link">ACTIVITIES</Link>
                            </li> */}
                        </ul>
                        <div className="navbar-text">
                            <Link to="/search"><i className="bi bi-search" style={{ marginRight: "15px" }}></i></Link>
                            <Link to="/profile"><i className="bi bi-person-fill" style={{ marginRight: "15px" }}></i></Link>
                            <Link to="/login"><i className="bi bi-box-arrow-right"></i></Link>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Header;