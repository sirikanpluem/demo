import React from 'react';
import { connect } from 'react-redux';
import { Link, Route, Routes } from "react-router-dom";
import ActivityDebtorPage from "./activities/activity-debtor.page"
import ActivityAddressPage from "./activities/activity-address.page";
import ActivityAssetDetailPage from "./activities/activity-assets-detail.page";
import ActivityDocumentPage from "./activities/activity-document.page";
import ActivityHomePage from "./activities/activity-home.page";
import ActivityAccountPage from "./activities/activity-account.page";
import ActivityLossRatioPage from "./activities/activity-loss-ratio.page";
import ActivityCapitalPage from "./activities/activity-capital.page";
import ActivityPropertyGradePage from "./activities/activity-property-grade.page";
import ActivitySueForEvictionPage from "./activities/activity-sue-for-eviction.page";
import ActivitySecurityRecordsPage from "./activities/activity-security-records.page";
import ActivityCutoffPage from "./activities/activity-cutoff.page";
import ActivityKeyLendingPage from "./activities/activity-key-lending.page";
import ActivityImpairmentPage from "./activities/activity-impairment.page";
import ActivityOfferPage from "./activities/activity-offer.page";
import ActivityContractPage from "./activities/activity-contract.page";
import ActivityGetMoneyPage from "./activities/activity-getmoney.page";
import ActivityOwnershipTransferPage from "./activities/activity-ownership-transfer.page";
import ActivityAccountCancelPage from "./activities/activity-account-cancel.page";
import ActivityAppaisalPricePage from "./activities/activity-appraisal-price.page";
import ActivityWithdrawCommonFeePage from "./activities/activity-withdraw-common-fee.page";
import ActivityPayChequeCommonFeePage from "./activities/activity-pay-cheque-common-fee.page";
import ActivityGetChequeCommonFeePage from "./activities/activity-get-cheque-common-fee.page";
import ActivityClearCommonFeePage from "./activities/activity-clear-common-fee.page";
import ActivityWithdrawLocalMaintenanceTaxPage from "./activities/activity-withdraw-local-maintenance-tax.pag";
import ActivityPayChequeMaintenanceTaxPage from "./activities/activity-pay-cheque-local-maintenance-tax.page";
import ActivityGetChequeMaintenanceTaxPage from "./activities/activity-get-cheque-local-maintenance-tax.page ";
import ActivityClearMaintenanceTaxPage from "./activities/activity-clear-local-maintenance-tax.page";
import ActivityDetailSueForEvictionPage from "./activities/activity-detail-sue-for-eviction.page";
import ActivitySellpropertyPage from "./activities/activity-sell-property.page";
import ActivityCleanerPage from "./activities/activity-cleaner.page";
export const menus = [
    {
        name: "ลูกหนี้",
        to: "/activity/debtor"
    },
    {
        name: "เอกสารสิทธิ์",
        to: "/activity/document"
    },
    {
        name: "ที่ตั้ง",
        to: "/activity/address"
    },
    {
        name: "รายละเอียดทรัพย์",
        to: "/activity/asset-detail"
    },
    {
        name: "ราคาประเมิน",
        to: "/activity/appraisal-price"
    },
    {
        name: "ต้นทุนทรัพย์",
        to: "/activity/capital"
    },
    // {
    //     name: "Update Loss Ratio",
    //     to: "/activity/loss-ratio"
    // },
    // {
    //     name: "บัญชี",
    //     to: "/activity/account"
    // },
    // {
    //     name: "ประกาศขายทรัพย์",
    //     to: "/activity/sell-property"
    // },
    // {
    //     name: "บันทึกเกรดทรัพย์",
    //     to: "/activity/property-grade"
    // },
    // {
    //     name: "บันทึกฟ้องขับไล่",
    //     to: "/activity/sue-for-eviction"
    // },
    // {
    //     name: "บันทึกข้อมูลรายละเอียดค่าใช้จ่าย การฟ้องขับไล่",
    //     to: "/activity/detail-sue-for-eviction"
    // },
    // {
    //     name: "บันทึก ค่ารปภ.",
    //     to: "/activity/security-records"
    // },
    // {
    //     name: "บันทึก ทะเบียนทำความสะอาด",
    //     to: "/activity/cleaner"
    // },
    // {
    //     name: "บันทึกทะเบียนกุญแจทรัพย์สิน",
    //     to: "/activity/key-lending"
    // },
    // {
    //     name: "Update Impairment",
    //     to: "/activity/impairment"
    // },
    // {
    //     name: "ค่าส่วนกลาง ค่าเบี้ยประกันภัย",
    //     children: [
    {
        name: "เบิกค่าส่วนกลาง",
        to: "/activity/withdraw-common-fee"
    },
    {
        name: "ออกเช็คค่าส่วนกลาง",
        to: "/activity/pay-cheque-common-fee"
    },
    {
        name: "รับเช็คค่าส่วนกลาง",
        to: "/activity/get-cheque-common-fee"
    },
    {
        name: "สั่งล้างรายการค่าส่วนกลาง",
        to: "/activity/clear-common-fee"
    },
    // ]
    // },
    // {
    //     name: "บันทึกทรัพย์มอบหมายพิเศษ",
    //     to: "/"
    // },
    // {
    //     name: "Update คะแนนผลงานขาย NPA",
    //     to: "/"
    // },
    // {
    //     name: "ค่าภาษีบำรุงท้องที่",
    //     children: [
    {
        name: "เบิกค่าภาษีบำรุงท้องที่",
        to: "/activity/withdraw-local-maintenance-tax"
    },
    {
        name: "ออกเช็คค่าภาษีบำรุงท้องที่",
        to: "/activity/pay-cheque-local-maintenance-tax"
    },
    {
        name: "รับเช็คค่าภาษีบำรุงท้องที่",
        to: "/activity/get-cheque-local-maintenance-tax"
    },
    {
        name: "สั่งล้างค่าภาษีบำรุงท้องที่",
        to: "/activity/clear-local-maintenance-tax"
    },
    // ]
    // },
    {
        name: "ตัดทะเบียนขายทรัพย์",
        to: "/activity/cutoff"
    },
    {
        name: "ข้อมูลเสนอซื้อ",
        to: "/activity/offer"
    },
    {
        name: "สัญญา",
        to: "/activity/contract"
    },
    {
        name: "การรับเงิน",
        to: "/activity/getmoney"
    },
    {
        name: "โอนกรรมสิทธิ์",
        to: "/activity/ownership-transfer"
    },
    {
        name: "ยกเลิกรายการขาย",
        to: "/activity/account-cancel"
    }
];

type Props = {};

type State = {};

class ActivityPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (

            <div className="container-fluid">

                <div className="row">
                    <div style={{ minHeight: "100vh", backgroundColor: "#875fc4", padding: 0, width: "250px" }}>
                        <div className="sticky-top sticky-margin" style={{ width: "250px", zIndex: 0, top: 0, bottom: 0, height: '100vh', overflow: 'hidden' }}>
                            <div style={{ padding: "20px 10px" }}>
                                <div style={{ color: "#feac00", fontSize: "25px", borderBottom: "2px solid #fdab00", paddingLeft: "10px", marginBottom: "20px" }}>
                                    ACTIVITIES
                                </div>
                                <div style={{ overflowY: 'auto', height: '84vh' }}>
                                    <div className="list-group">
                                        {
                                            menus.map((menu, index) => (
                                                <Link key={index} to={menu.to ? menu.to : "/"} className="list-group-item list-group-item-action">
                                                    {menu.name}
                                                </Link>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <div className="sticky-margin">
                            <Routes>
                                <Route path="/*" element={<ActivityHomePage />} />
                                <Route path="/debtor" element={<ActivityDebtorPage />} />
                                <Route path="/document" element={<ActivityDocumentPage />} />
                                <Route path="/address" element={<ActivityAddressPage />} />
                                <Route path="/asset-detail" element={<ActivityAssetDetailPage />} />
                                <Route path="/account" element={<ActivityAccountPage />} />
                                <Route path="/loss-ratio" element={<ActivityLossRatioPage />} />
                                <Route path="/capital" element={<ActivityCapitalPage />} />
                                <Route path="/property-grade" element={<ActivityPropertyGradePage />} />
                                <Route path="/sue-for-eviction" element={<ActivitySueForEvictionPage />} />
                                <Route path="/security-records" element={<ActivitySecurityRecordsPage />} />
                                <Route path="/cutoff" element={<ActivityCutoffPage />} />
                                <Route path="/key-lending" element={<ActivityKeyLendingPage />} />
                                <Route path="/impairment" element={<ActivityImpairmentPage />} />
                                <Route path="/offer" element={<ActivityOfferPage />} />
                                <Route path="/contract" element={<ActivityContractPage />} />
                                <Route path="/getmoney" element={<ActivityGetMoneyPage />} />
                                <Route path="/ownership-transfer" element={<ActivityOwnershipTransferPage />} />
                                <Route path="/account-cancel" element={<ActivityAccountCancelPage />} />
                                <Route path="/appraisal-price" element={<ActivityAppaisalPricePage />} />
                                <Route path="/withdraw-common-fee" element={<ActivityWithdrawCommonFeePage />} />
                                <Route path="/pay-cheque-common-fee" element={<ActivityPayChequeCommonFeePage />} />
                                <Route path="/get-cheque-common-fee" element={<ActivityGetChequeCommonFeePage />} />
                                <Route path="/clear-common-fee" element={<ActivityClearCommonFeePage />} />
                                <Route path="/withdraw-local-maintenance-tax" element={<ActivityWithdrawLocalMaintenanceTaxPage />} />
                                <Route path="/pay-cheque-local-maintenance-tax" element={<ActivityPayChequeMaintenanceTaxPage />} />
                                <Route path="/get-cheque-local-maintenance-tax" element={<ActivityGetChequeMaintenanceTaxPage />} />
                                <Route path="/clear-local-maintenance-tax" element={<ActivityClearMaintenanceTaxPage />} />
                                <Route path="/detail-sue-for-eviction" element={<ActivityDetailSueForEvictionPage />} />
                                <Route path="/sell-property" element={<ActivitySellpropertyPage />} />
                                <Route path="/cleaner" element={<ActivityCleanerPage />} />
                            </Routes>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityPage);