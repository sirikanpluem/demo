import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ReportTestPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "20px" }}>
                        <span>ทะเบียนเบิกค่าใช้จ่ายในการฟ้องขับไล่</span>
                    </h5>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ReportTestPage);