import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ReportContractTransferMaturityPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "20px" }}>
                        <span>รายงานทรัพย์สินรับโอนชำระหนี้</span>
                    </h5>
                </div>
                <div className="card mb-3">
                    <div className="card-body">
                        <form className="row align-items-center">
                            <div className="col-12 col-md-6">
                                <div className="row">
                                    <label className="col-md-4 col-form-label text-md-end">
                                        ชื่อลูกหนี้
                                    </label>
                                    <div className="col-md-8">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="staticEmail"
                                            autoComplete="off"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-5">
                                <div className="row">
                                    <label className="col-md-4 col-form-label text-md-end">
                                        เอกสารสิทธิ์
                                    </label>
                                    <div className="col-md-8">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="staticEmail"
                                            autoComplete="off"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-1 text-center">
                                <button
                                    className="btn btn-primary rounded-circle m-1"
                                    style={{ height: 50, width: 50 }}
                                >
                                    <i className="bi bi-search" style={{ marginRight: "15px" }}></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div style={{ overflowX: 'auto', width: '174vh'}}>
                    <table className="table table-bordered mb-3">
                        <thead>
                            <tr>
                                <th>การติดตาม</th>
                                <th>ชื่อผู้ชื้อ</th>
                                <th>ทรัพย์สินรับโอนราย</th>
                                <th>ผู้รับผิดชอบการฟ้อง</th>
                                <th>ฟ้องขับไล่</th>
                                <th>ระยะเวลา</th>
                                <th>ราคาขาย</th>
                                <th>วันที่ครบกำหนดสัญญา</th>
                                <th>วันที่ทำสัญญา</th>
                                <th>วันที่บันทึกบัญชีทำสัญญา</th>
                                <th>สถานะทรัพย์</th>
                                <th>สำนักงานเขต</th>
                                <th>สาขา</th>
                                <th>เอกสารสิทธิ์</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ReportContractTransferMaturityPage);