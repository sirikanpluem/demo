import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ReportSellPropertyPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "20px" }}>
                        <span>ขายทรัพย์</span>
                    </h5>
                </div>
                <div className="card mb-3">
                    <div className="card-body">
                        <form className="row align-items-center">
                            <div className="col-12 col-md-6">
                                <div className="row">
                                    <label className="col-md-4 col-form-label text-md-end">
                                        ชื่อลูกหนี้
                                    </label>
                                    <div className="col-md-8">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="staticEmail"
                                            autoComplete="off"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-5">
                                <div className="row">
                                    <label className="col-md-4 col-form-label text-md-end">
                                        เอกสารสิทธิ์
                                    </label>
                                    <div className="col-md-8">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="staticEmail"
                                            autoComplete="off"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-1 text-center">
                                <button
                                    className="btn btn-primary rounded-circle m-1"
                                    style={{ height: 50, width: 50 }}
                                >
                                    <i className="bi bi-search" style={{ marginRight: "15px" }}></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div style={{ overflowX: 'auto', width: '174vh'}}>
                    <table className="table table-bordered mb-3">
                        <thead>
                            <tr>
                                <th>ชื่อลูกหนี้</th>
                                <th>ประเภท</th>
                                <th>สาขา</th>
                                <th>เขต</th>
                                <th>ที่ตั้ง</th>
                                <th>วันที่ขาย</th>
                                <th>ประมูลซื้อ</th>
                                <th>วันที่ได้มา</th>
                                <th>เอกสารสิทธิ์</th>
                                <th>เนื้อที่</th>
                                <th>ต้นทุนทรัพย์</th>
                                <th>ราคาขาย</th>
                                <th>ค่าโอนขาย</th>
                                <th>ภาษีนิติฯ</th>
                                <th>ภาษีธุรกิจฯ</th>
                                <th>เงินรางวัล</th>
                                <th>ผู้รับเงินรางวัล</th>
                                <th>เลขที่บัญชี</th>
                                <th>กำไร</th>
                                <th>ผู้ซื้อ</th>
                                <th>%Loss Out</th>
                                <th>เงินต้น</th>
                                <th>ดอกเบี้ย 1</th>
                                <th>ค่าใช้จ่าย</th>
                                <th>การได้มา</th>
                                <th>เกรด</th>
                                <th>สาขาที่ขายได้</th>
                                <th>ราคาประเมิน</th>
                                <th>วันที่ประเมิน</th>
                                <th>บันทึกบัญชีรับโอน</th>
                                <th>บันทึกบัญชีทำสัญญา</th>
                                <th>บันทึกบัญชีขาย</th>
                                <th>ประเภทผู้ซื้อ</th>
                                <th>แปลง</th>
                                <th>Forcedsale</th>
                                <th>ประเภทสินเชื่อ</th>
                                <th>ขนาดทรัพย์</th>
                                <th>กลุ่มทรัพย์</th>
                                <th>ช่องทางการขาย</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ReportSellPropertyPage);