import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ReportTenderOfferPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "20px" }}>
                        <span>รายงานข้อมูลการเสนอซื้อทรัพย์</span>
                    </h5>
                </div>
                <div className="card mb-3">
                    <div className="card-body">
                        <form className="row align-items-center">
                            <div className="col-12 col-md-6">
                                <div className="row">
                                    <label className="col-md-4 col-form-label text-md-end">
                                        ชื่อลูกหนี้
                                    </label>
                                    <div className="col-md-8">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="staticEmail"
                                            autoComplete="off"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-5">
                                <div className="row">
                                    <label className="col-md-4 col-form-label text-md-end">
                                        เอกสารสิทธิ์
                                    </label>
                                    <div className="col-md-8">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="staticEmail"
                                            autoComplete="off"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-1 text-center">
                                <button
                                    className="btn btn-primary rounded-circle m-1"
                                    style={{ height: 50, width: 50 }}
                                >
                                    <i className="bi bi-search" style={{ marginRight: "15px" }}></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div style={{ overflowX: 'auto', width: '174vh'}}>
                    <table className="table table-bordered mb-3">
                        <thead>
                            <tr>
                                <th>AO</th>
                                <th>การได้มาซึ่งกรรมสิทธิ์</th>
                                <th>เกรด</th>
                                <th>ค่าใช้จ่ายรับโอนกรรมสิทธิ์</th>
                                <th>ค่าใช้จ่ายอื่น ๆ</th>
                                <th>ค่าภาษีบำรุงท้องที่/โรงเรือน</th>
                                <th>เครื่องปรับอากาศ</th>
                                <th>เงินต้น</th>
                                <th>จำนวนชั้น</th>
                                <th>จำนวนรถที่จอดในโรงรถได้</th>
                                <th>จำนวนห้องคนรับใช้</th>
                                <th>จำนวนห้องครัว</th>
                                <th>จำนวนห้องนอน</th>
                                <th>จำนวนห้องนั่งเล่น</th>
                                <th>จำนวนห้องน้ำ</th>
                                <th>จำนวนห้องรับแขก</th>
                                <th>จำนวนห้องอาหาร</th>
                                <th>ชั้นที่</th>
                                <th>ดอกเบี้ยค้างรับ 1</th>
                                <th>ตกแต่งพร้อมอยู่</th>
                                <th>ต้นทุนทรัพย์</th>
                                <th>ที่ดินหน้ากว้างติดถนน (ม.)</th>
                                <th>ที่ตั้ง</th>
                                <th>โทรศัพท์</th>
                                <th>น้ำ</th>
                                <th>เนื้อที่ (ไร่-งาน-ตารางวา)</th>
                                <th>ประเภททรัพย์</th>
                                <th>ประเภทหนี้</th>
                                <th>ผู้ทำรายการ</th>
                                <th>ฝ่ายต้นสังกัด</th>
                                <th>พื้นที่สนามหญ้า (ตารางวา)</th>
                                <th>เฟอร์นิเจอร์เพิ่มเติม</th>
                                <th>ไฟฟ้า</th>
                                <th>ภาระผูกพันกับธนาคาร</th>
                                <th>ภาษีการขาย</th>
                                <th>ภาษีค่าธรรมเนียม</th>
                                <th>ภาษีธุรกิจเฉพาะ</th>
                                <th>รหัสทรัพย์</th>
                                <th>ระวาง</th>
                                <th>ราคา Forced Sale</th>
                                <th>ราคาขายทอดตลาด</th>
                                <th>ราคาขายในงาน</th>
                                <th>ราคาขายปกติ</th>
                                <th>ราคาประเมินเชิงพาณิชย์</th>
                                <th>ราคาประเมินราชการ</th>
                                <th>รายละเอียด</th>
                                <th>ลานซักล้าง</th>
                                <th>ลูกหนี้ของสาขา</th>
                                <th>เลขที่อ้างอิง</th>
                                <th>วงเงินกู้</th>
                                <th>วันที่ทำรายการ</th>
                                <th>วันที่ประมูลซื้อทรัพย์</th>
                                <th>วันที่ประเมินเชิงพาณิชย์</th>
                                <th>วันที่รับโอนเอกสารสิทธิ์</th>
                                <th>สนามหญ้า</th>
                                <th>เอกสารสิทธิ์</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                                <td>XXXX</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ReportTenderOfferPage);