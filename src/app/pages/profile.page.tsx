import React from 'react';
import { connect } from 'react-redux';

type Props = {};

type State = {};

class ProfilePage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <>
                <div className="container py-3">
                    ProfilePage
                </div>
            </>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);