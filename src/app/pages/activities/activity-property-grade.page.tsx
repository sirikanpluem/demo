import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ActivityPropertyGradePage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "50px" }}>บันทึกเกรดทรัพย์
                    <select className="form-select float-end" style={{ maxWidth: "250px" }} onChange={e => this.setState({
                            view: e.target.value
                        })}>
                            <option value="DEFAULT_VIEW">Default View</option>
                            <option value="ASSET_VIEW">รายการรับเงิน</option>
                            <option value="DEFAULT_VIEW1">สลิปตีโอนชำระหนี้/ประมูลซื้อ</option>
                            <option value="ASSET_VIEW2">สลิปเงินทดลอง</option>
                            <option value="DEFAULT_VIEW3">สลิปลดทุน/เพิ่มทุน</option>
                            <option value="ASSET_VIEW4">สลิปยกเลิกการขายคืนเงิน</option>
                            <option value="ASSET_VIEW4">สลิปโอนขาย</option>
                    </select>
                    </h5>
                    <form className="row">

                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityPropertyGradePage);