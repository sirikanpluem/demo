import React from 'react';
import { connect } from 'react-redux';
import  NewDebtor  from "../../components/new-debtor";
type Props = {};

type State = {};

class ActivityDebtorPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <NewDebtor />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityDebtorPage);