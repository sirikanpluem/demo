import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ActivityAssetDetailPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "50px" }}>บันทึกต้นทุนทรัพย์
                    <select className="form-select float-end" style={{ maxWidth: "250px" }} onChange={e => this.setState({
                            view: e.target.value
                        })}>
                            <option value="DEFAULT_VIEW">Default View</option>
                            <option value="ASSET_VIEW">รายการรับเงิน</option>
                            <option value="DEFAULT_VIEW1">สลิปตีโอนชำระหนี้/ประมูลซื้อ</option>
                            <option value="ASSET_VIEW2">สลิปเงินทดลอง</option>
                            <option value="DEFAULT_VIEW3">สลิปลดทุน/เพิ่มทุน</option>
                            <option value="ASSET_VIEW4">สลิปยกเลิกการขายคืนเงิน</option>
                            <option value="ASSET_VIEW4">สลิปโอนขาย</option>
                    </select>
                    </h5>
                    <form className="row">
                        <h6>ปรับปรุงค่าใช้จ่ายประมูลซื้อ/รับโอนกรรมสิทธิ์</h6>
                        <div className="card py-3 mb-3">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ปรับปรุงค่าใช้จ่าย</label>
                                        <div className="col-sm-8">
                                            <select className="form-select" >
                                                <option>ประมูลซื้อ</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วิธีคำนวณ</label>
                                        <div className="col-sm-8">
                                            <select className="form-select" >
                                                <option>เพิ่ม</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนเงิน</label>
                                        <div className="col-sm-8">
                                            <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                        </div>
                                    </div>

                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">หมายเหตุ</label>
                                        <div className="col-sm-8">
                                            <textarea className="form-control" id="staticEmail" rows={3}></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h6 >บันทึกข้อมูลสำรองค่าใช้จ่าย</h6>
                        <div className="card py-3 mb-3">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ประเภท</label>
                                        <div className="col-sm-8">
                                            <select className="form-select" >
                                                <option>สำรองค่าใช้จ่ายตีโอน</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วันที่ดำเนินการ</label>
                                        <div className="col-sm-8">
                                            <select className="form-select" >
                                                <option>yyyy/mm/dd</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนเงิน</label>
                                        <div className="col-sm-8">
                                            <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">โอนเข้าบัญชี</label>
                                        <div className="col-sm-8">
                                            <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เลขที่บัญชี</label>
                                        <div className="col-sm-8">
                                            <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เลขคุมบัญชีพัก</label>
                                        <div className="col-sm-8">
                                            <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="mb-3 row">
                                        <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">หมายเหตุ</label>
                                        <div className="col-sm-8">
                                            <textarea className="form-control" id="staticEmail" rows={3}></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="row" style={{ width: "100%", maxWidth: "320px", float: "right" }}>
                                <div className="col-6">
                                    <button type="reset" className="btn btn-primary w-100">บันทึก</button>
                                </div>
                                <div className="col-6">
                                    <button type="submit" className="btn btn-secondary w-100">ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityAssetDetailPage);