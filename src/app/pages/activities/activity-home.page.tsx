import React from 'react';
import { connect } from 'react-redux';
import { Navigate, NavLink, Route, Routes } from "react-router-dom";
import ActivityHistoryPage from "./activity-history.page";
import ActivityInformationPage from "./activity-information.page";
type Props = {};

type State = {};

class ActivityHomePage extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            tabIndex: 0
        };

        this.onChangeTabs = this.onChangeTabs.bind(this);
    }

    onChangeTabs(e: any, index: any) {
        // e.preventDefault();
        this.setState({
            tabIndex: index
        });
    }
    render(): React.ReactNode {
        return (
            <div>
                <ul className="nav nav-tabs nav-item-groups">
                    <li className="nav-item">
                        <NavLink to={'information'} className="nav-link">Information</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to={'history'} className="nav-link">History</NavLink>
                    </li>
                </ul >

                <Routes>
                    <Route path="/" element={<Navigate replace to="information" />} />
                    <Route path="/information" element={<ActivityInformationPage />} />
                    <Route path="/history" element={<ActivityHistoryPage />} />
                </Routes>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityHomePage);