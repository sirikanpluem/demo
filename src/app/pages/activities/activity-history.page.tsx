import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ActivityHistoryPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "20px" }}>
                        <span>AMIS Default History</span>
                    </h5>
                </div>
                <div className="container">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th>Date Time</th>
                                <th>Activity</th>
                                <th>User</th>
                                <th>Summary	view</th>
                                <th>view</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>30/11/2021 3:48:00 PM</td>
                                <td>บันทึกลูกหนี้</td>
                                <td>Suphannee</td>
                                <td>New Debtor</td>
                                <td>
                                    <a href="/"><i className="bi bi-file-earmark-text"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityHistoryPage);