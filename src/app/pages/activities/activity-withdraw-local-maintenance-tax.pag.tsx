import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ActivityWithdrawLocalMaintenanceTaxPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
            <div className="container">
                <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "50px" }}>บันทึกเบิกค่าภาษีบำรุงท้องที่
                <select className="form-select float-end" style={{ maxWidth: "250px" }} onChange={e => this.setState({
                            view: e.target.value
                        })}>
                            <option value="DEFAULT_VIEW">Default View</option>
                            <option value="ASSET_VIEW">รายการรับเงิน</option>
                            <option value="DEFAULT_VIEW1">สลิปตีโอนชำระหนี้/ประมูลซื้อ</option>
                            <option value="ASSET_VIEW2">สลิปเงินทดลอง</option>
                            <option value="DEFAULT_VIEW3">สลิปลดทุน/เพิ่มทุน</option>
                            <option value="ASSET_VIEW4">สลิปยกเลิกการขายคืนเงิน</option>
                            <option value="ASSET_VIEW4">สลิปโอนขาย</option>
                    </select>
                </h5>
                <form className="row">
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">รูปแบบการจ่ายเงิน</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">สั่งจ่ายในนาม</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ชื่อบัญชี</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เลขที่บัญชี</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนเงินค่าส่วนกลาง</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนเงินค่าเบี้ยประกันภัย</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">R/C</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วันที่ครบกำหนด</label>
                            <div className="col-sm-8">
                                <input type="date" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วันที่รับใบแจ้งหนี้</label>
                            <div className="col-sm-8">
                                <input type="date" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ผู้โทรติดตามใบแจ้งหนี้</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                </div>
                <div className="col-12">
                        <div className="row" style={{ width: "100%", maxWidth: "320px", float: "right" }}>
                            <div className="col-6">
                                <button type="reset" className="btn btn-primary w-100">บันทึก</button>
                            </div>
                            <div className="col-6">
                                <button type="submit" className="btn btn-secondary w-100">ยกเลิก</button>
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityWithdrawLocalMaintenanceTaxPage);