import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ActivityApproveAccountPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "50px" }}>อนุมัติรายการนำส่งระบบบัญชี
                    <select className="form-select float-end" style={{ maxWidth: "250px" }} onChange={e => this.setState({
                        view: e.target.value
                    })}>
                            <option value="DEFAULT_VIEW">Default View</option>
                            <option value="ASSET_VIEW">รายการรับเงิน</option>
                            <option value="DEFAULT_VIEW1">สลิปตีโอนชำระหนี้/ประมูลซื้อ</option>
                            <option value="ASSET_VIEW2">สลิปเงินทดลอง</option>
                            <option value="DEFAULT_VIEW3">สลิปลดทุน/เพิ่มทุน</option>
                            <option value="ASSET_VIEW4">สลิปยกเลิกการขายคืนเงิน</option>
                            <option value="ASSET_VIEW4">สลิปโอนขาย</option>
                        </select>
                    </h5>
                    <div className="row">
                        <div className="container">
                            <div>
                                <div className="card py-3 mb-3 align-items-center" >
                                    <h2>WAIT REPORT GL</h2>
                                    <h2>WAIT REPORT GL</h2>
                                    <h2>WAIT REPORT GL</h2>
                                    <h2>WAIT REPORT GL</h2>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="checkbox" name="type" id="approve" value="APPROVE" 
                                    style={{ width: "30px" , height: "30px"}}
                                    onChange={(e) => this.setState({
                                        type: "CHECK"
                                    })} defaultChecked />
                                    <label className="form-check-label" htmlFor="approve" style={{ paddingTop: "8px" , paddingLeft:"13px" }}>
                                        <h6>อนุมัติรายการนำส่งระบบบัญชี</h6>
                                     </label>
                                </div>
                                <div className="col-12">
                            <div className="row" style={{ width: "100%", maxWidth: "320px", float: "right" }}>
                                <div className="col-6">
                                    <button type="reset" className="btn btn-primary w-100">บันทึก</button>
                                </div>
                                <div className="col-6">
                                    <button type="submit" className="btn btn-secondary w-100">ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityApproveAccountPage);