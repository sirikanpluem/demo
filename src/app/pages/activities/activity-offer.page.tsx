import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ActivityOfferPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "50px" }}>บันทึกข้อมูลเสนอซื้อ
                    <select className="form-select float-end" style={{ maxWidth: "250px" }} onChange={e => this.setState({
                            view: e.target.value
                        })}>
                            <option value="DEFAULT_VIEW">Default View</option>
                            <option value="ASSET_VIEW">รายการรับเงิน</option>
                            <option value="DEFAULT_VIEW1">สลิปตีโอนชำระหนี้/ประมูลซื้อ</option>
                            <option value="ASSET_VIEW2">สลิปเงินทดลอง</option>
                            <option value="DEFAULT_VIEW3">สลิปลดทุน/เพิ่มทุน</option>
                            <option value="ASSET_VIEW4">สลิปยกเลิกการขายคืนเงิน</option>
                            <option value="ASSET_VIEW4">สลิปโอนขาย</option>
                    </select>
                    </h5>
                    <form className="row">
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วันที่เสนอซื้อ</label>
                                <div className="col-sm-8">
                                    <input type="date" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ชื่อ-นามสกุลผู้ซื้อ</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ชื่อบริษัท</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">หมายเลขโทรศัพท์</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เลขที่</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ภูมิภาค (Region)</label>
                                <div className="col-sm-8">
                                    <select className="form-select" >
                                        <option>Open this select menu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">หมู่ที่</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จังหวัด</label>
                                <div className="col-sm-8">
                                    <select className="form-select" >
                                        <option>Open this select menu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ชื่อหมู่บ้าน/อาคาร</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">อำเภอ</label>
                                <div className="col-sm-8">
                                    <select className="form-select" >
                                        <option>Open this select menu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ซอย</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ตำบล</label>
                                <div className="col-sm-8">
                                    <select className="form-select" >
                                        <option>Open this select menu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ถนน</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">รหัสไปรษณีย์</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เบอร์โทรศัพท์</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">โทรสาร</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">E-mail</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">แนะนำโดย</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ราคาเสนอซื้อทรัพย์</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">หมายเหตุ</label>
                                <div className="col-sm-8">
                                    <textarea className="form-control" id="staticEmail" rows={2}></textarea>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-3 row">
                                <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วัตถุประสงค์ในการซื้อ</label>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                </div>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="row" style={{ width: "100%", maxWidth: "320px", float: "right" }}>
                                <div className="col-6">
                                    <button type="reset" className="btn btn-primary w-100">บันทึก</button>
                                </div>
                                <div className="col-6">
                                    <button type="submit" className="btn btn-secondary w-100">ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityOfferPage);