import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ActivityAppaisalPricePage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
            <div className="container">
                <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "50px" }}>บันทึกรายละเอียดทรัพย์
                <select className="form-select float-end" style={{ maxWidth: "250px" }} onChange={e => this.setState({
                            view: e.target.value
                        })}>
                            <option value="DEFAULT_VIEW">Default View</option>
                            <option value="ASSET_VIEW">รายการรับเงิน</option>
                            <option value="DEFAULT_VIEW1">สลิปตีโอนชำระหนี้/ประมูลซื้อ</option>
                            <option value="ASSET_VIEW2">สลิปเงินทดลอง</option>
                            <option value="DEFAULT_VIEW3">สลิปลดทุน/เพิ่มทุน</option>
                            <option value="ASSET_VIEW4">สลิปยกเลิกการขายคืนเงิน</option>
                            <option value="ASSET_VIEW4">สลิปโอนขาย</option>
                    </select>
                </h5>
                <form className="row">
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ชั้นที่ (ห้องชุด)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนชั้น (ชั้น)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ที่ดินหน้ากว้างติดถนน (m.)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ขนาดพื้นที่ใช้สอย (ตรม.)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้อง (ห้อง)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้องน้ำ (ห้อง)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้องรับแขก (ห้อง)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้องทำงาน (ห้อง)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้องนั่งเล่น (ห้อง)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้องคนรับใช้ (ห้อง)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้องอาหาร (ห้อง)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนที่จอดรถ (คัน)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้องครัว (ห้อง)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้องพักคนงาน (ห้อง)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">น้ำประปา</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ตกแต่งพร้อมอยู่</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ไฟฟ้า</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ลานซักล้าง</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">แอร์</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">สนามหญ้า</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">โทรศัพท์</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">พื้นที่สนามหญ้า (ตรม.)</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ภาระผูกพันธ์กับธนาคาร</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เฟอร์นิเจอร์</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ถนนหลัก</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">การล้อมรั้ว</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">การถือครองกรรมสิทธิ์</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เครื่องใช้ไฟฟ้า</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">มินิมาร์ท/ร้านขายของ/ร้านค้า</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">สระว่ายน้ำส่วนกลาง</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">คลับเฮาส์ส่วนกลาง</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">พื้นที่ส่วนของออฟฟิศ</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ฟิตเนสส่วนกลาง</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">พื้นที่ส่วนต้อนรับ/Lobby</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">สนามกีฬากลางแจ้ง</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนห้องเรียน</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">สนามกีฬาในร่ม</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">Note รายละเอียดทรัพย์</label>
                            <div className="col-sm-8">
                                <textarea className="form-control" id="staticEmail" rows={3}></textarea>

                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนอาคาร</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนหัวจ่ายน้ำมัน</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row" style={{ width: "100%", maxWidth: "320px", float: "right" }}>
                            <div className="col-6">
                                <button type="reset" className="btn btn-primary w-100">บันทึก</button>
                            </div>
                            <div className="col-6">
                                <button type="submit" className="btn btn-secondary w-100">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>  
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityAppaisalPricePage);