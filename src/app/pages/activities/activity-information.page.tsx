import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {
    view: string,
};

class ActivityInformationPage extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            view: 'DEFAULT_VIEW'
        };
    }
    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
                <div className="container">
                    <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "20px" }}>
                        <span>Reference Number :</span>
                        <select className="form-select float-end" style={{ maxWidth: "250px" }} onChange={e => this.setState({
                            view: e.target.value
                        })}>
                            <option value="DEFAULT_VIEW">Default View</option>
                            <option value="ASSET_VIEW">Asset View</option>
                        </select>
                    </h5>
                </div>
                {this.state.view === "DEFAULT_VIEW" ?
                    <div className="container">
                        <div className="card py-3 mb-3">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">Reference Number</label>
                                        <div className="col-sm-8 col-form-label">53482</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">เลขที่บัญชี</label>
                                        <div className="col-sm-8 col-form-label">นายสมคิด สนุกยิ่ง</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">ชื่อลูกหนี้</label>
                                        <div className="col-sm-8 col-form-label">นางอำไร รักสงบ</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">ลูกหนี้ของสาขา</label>
                                        <div className="col-sm-8 col-form-label">นางพนิต  ชื่อชอบ</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">วงเงินกู้</label>
                                        <div className="col-sm-8 col-form-label">500,000.00</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">การได้มาซึ่งกรรมสิทธิ์</label>
                                        <div className="col-sm-8 col-form-label">XX</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">เงินต้น</label>
                                        <div className="col-sm-8 col-form-label">500,000.00</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">ประเภทหนี้</label>
                                        <div className="col-sm-8 col-form-label">XX</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">ดอกเบี้ยค้างรับ1</label>
                                        <div className="col-sm-8 col-form-label">32,121.30</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">ผู้ทำรายการ</label>
                                        <div className="col-sm-8 col-form-label">นายสมคิด สนุกยิ่ง</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">ดอกเบี้ยค้างรับ2</label>
                                        <div className="col-sm-8 col-form-label">11,231.10</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">วันที่ทำรายการ</label>
                                        <div className="col-sm-8 col-form-label">21/01/2022</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">ค่าใช้จ่ายอื่นๆ</label>
                                        <div className="col-sm-8 col-form-label">-</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5 style={{ paddingBottom: "10px" }}>Related Asset</h5>
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th>เอกสารสิทธิ์</th>
                                    <th>ประเภททรัพย์</th>
                                    <th>ต้นทุนทรัพย์</th>
                                    <th>ราคาเสนอขาย</th>
                                    <th>ที่ตั้ง</th>
                                    <th>view</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>โฉนดเลขที่ 16808</td>
                                    <td>ที่ดินว่างเปล่า - ในเมือง</td>
                                    <td>999,999.99</td>
                                    <td>1,000,000.00</td>
                                    <td>เชียงราย-เชียงใหม่ แม่เจดีย์ใหม่ เวียงป่าเป้า</td>
                                    <td>
                                        <a href="/"><i className="bi bi-file-earmark-text"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    :
                    <div className="container">
                        <h5>General Detail</h5>
                        <div className="card py-3 mb-3">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">เอกสารสิทธิ์</label>
                                        <div className="col-sm-8 col-form-label">โฉนด</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            ประเภททรัพย์สิน</label>
                                        <div className="col-sm-8 col-form-label">บ้าน</div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            เลขเอกสารสิทธิ์</label>
                                        <div className="col-sm-8 col-form-label">31243</div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            เนื้อที่ ไร่</label>
                                        <div className="col-sm-8 col-form-label">10</div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            งาน</label>
                                        <div className="col-sm-8 col-form-label">2</div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            วา</label>
                                        <div className="col-sm-8 col-form-label">9</div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">ตารางเมตร</label>
                                        <div className="col-sm-8 col-form-label">900</div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">ระวาง</label>
                                        <div className="col-sm-8 col-form-label">300</div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">จำนวนแปลง</label>
                                        <div className="col-sm-8 col-form-label">9</div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">วันที่รับโอนกรรมสิทธิ์</label>
                                        <div className="col-sm-8 col-form-label">21/01/2022</div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">วันที่ส่งบัญชีรับโอน</label>
                                        <div className="col-sm-8 col-form-label">15/01/2022</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">รายละเอียดทรัพย์สิน</label>
                                        <div className="col-sm-8 col-form-label">บ้านเปล่า</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5 >Address</h5>
                        <div className="card py-3 mb-3">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            เลขที่
                                        </label>
                                        <div className="col-sm-8 col-form-label">39</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            ภูมิภาค (Region)
                                        </label>
                                        <div className="col-sm-8 col-form-label">XX</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            หมู่ที่
                                        </label>
                                        <div className="col-sm-8 col-form-label">2</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            จังหวัด
                                        </label>
                                        <div className="col-sm-8 col-form-label">กรุงเทพมหานคร</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            ชื่อหมู่บ้าน/อาคาร
                                        </label>
                                        <div className="col-sm-8 col-form-label">อารียา</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            อำเภอ
                                        </label>
                                        <div className="col-sm-8 col-form-label">สามพร้าว</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            ซอย
                                        </label>
                                        <div className="col-sm-8 col-form-label">สุขุมวิท 3</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            ตำบล
                                        </label>
                                        <div className="col-sm-8 col-form-label">สุขุมวิท</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            ถนน
                                        </label>
                                        <div className="col-sm-8 col-form-label">สุขุมวิท 32</div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="row">
                                        <label className="col-sm-4 col-form-label text-end">
                                            รหัสไปรษณีย์
                                        </label>
                                        <div className="col-sm-8 col-form-label">10900</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h5>Asset Detail</h5>
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityInformationPage);