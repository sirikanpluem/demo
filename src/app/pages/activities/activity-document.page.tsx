import React from 'react';
import { connect } from 'react-redux';
type Props = {};

type State = {};

class ActivityDocumentPage extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            view: 'DEFAULT_VIEW'
        };
    }

    render(): React.ReactNode {
        return (
            <div style={{ paddingTop: "50px" }}>
            <div className="container">
                <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "50px" }}>บันทึกเอกสารสิทธิ์
                <select className="form-select float-end" style={{ maxWidth: "250px" }} onChange={e => this.setState({
                            view: e.target.value
                        })}>
                            <option value="DEFAULT_VIEW">Default View</option>
                            <option value="ASSET_VIEW">รายการรับเงิน</option>
                            <option value="DEFAULT_VIEW1">สลิปตีโอนชำระหนี้/ประมูลซื้อ</option>
                            <option value="ASSET_VIEW2">สลิปเงินทดลอง</option>
                            <option value="DEFAULT_VIEW3">สลิปลดทุน/เพิ่มทุน</option>
                            <option value="ASSET_VIEW4">สลิปยกเลิกการขายคืนเงิน</option>
                            <option value="ASSET_VIEW4">สลิปโอนขาย</option>
                </select>
                {/* <button className="form-control float-end" style={{ maxWidth: "150px" }}>xxxx</button> */}
                </h5>
                <form className="row">
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เอกสารสิทธิ์</label>
                            <div className="col-sm-8">
                                <select className="form-select" >
                                    <option>Open this select menu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ประเภททรัพย์สิน</label>
                            <div className="col-sm-8">
                                <select className="form-select" >
                                    <option>Open this select menu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เลขเอกสารสิทธิ์</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end"></label>
                            <div className="col-sm-8">
                                <select className="form-select" >
                                    <option>Open this select menu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">เนื้อที่ ไร่</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end"></label>
                            <div className="col-sm-8">
                                <select className="form-select" >
                                    <option>Open this select menu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">งาน</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วันที่รับโอนกรรมสิทธิ์</label>
                            <div className="col-sm-8">
                                <input type="date" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วา</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">วันที่ส่งบัญชีรับโอน</label>
                            <div className="col-sm-8">
                                <input type="date" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ตารางเมตร</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">รายละเอียดทรัพย์สิน</label>
                            <div className="col-sm-8">
                                <textarea className="form-control" id="staticEmail" rows={3}></textarea>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">ระวาง</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <div className="mb-3 row">
                            <label htmlFor="staticEmail" className="col-sm-4 col-form-label text-end">จำนวนแปลง</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row" style={{ width: "100%", maxWidth: "320px", float: "right" }}>
                            <div className="col-6">
                                <button type="reset" className="btn btn-primary w-100">บันทึก</button>
                            </div>
                            <div className="col-6">
                                <button type="submit" className="btn btn-secondary w-100">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityDocumentPage);