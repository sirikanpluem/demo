import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";

export const menus = [
    {
        name: "บัญชีจากตีโอนชำระหนี้",
        type: "repayment",
        count: 1
    },
    {
        name: "บัญชีจากประมูลซื้อทรัพย์",
        type: "bid",
        count: 2
    },
    {
        name: "บัญชีรับชำระเงิน - อสังหาริมทรัพย์",
        type: "real-estate",
        count: 3
    },
    {
        name: "บัญชีรับชำระเงิน - สังหาริมทรัพย์",
        type: "chattel",
        count: 3
    },
    {
        name: "บัญชีริบเงินประกัน",
        type: "get-bail",
        count: 1
    },
    {
        name: "บัญชีคืนเงินประกัน",
        type: "return-bail",
        count: 1
    },
    {
        name: "บัญชีโอนขาย",
        type: "transfer",
        count: 12
    },
    {
        name: "บัญชีโอนขายฯ - ผู้ซื้อรับภาระค่าภาษีธุรกิจเฉพาะ",
        type: "bu-tax",
        count: 1
    },
    {
        name: "ออกเช็คค่าส่วนกลาง",
        type: "cheque-common-fee",
        count: 1
    },
    {
        name: "ล้างบัญชีพักค่าส่วนกลาง",
        type: "clear-common-fee",
        count: 15
    },
    {
        name: "รับคืนค่าส่วนกลาง",
        type: "take-back-common-fee",
        count: 2
    },
    {
        name: "ออกเช็คค่าภาษีบำรุงท้องที่",
        type: "cheque-local-maintenance-tax",
        count: 10
    },
    {
        name: "ล้างบัญชีพักออกเช็คค่าภาษีบำรุงท้องที่",
        type: "clear-local-maintenance-tax",
        count: 10
    },
    {
        name: "บัญชีค่าเช่า",
        type: "rent",
        count: 15
    }
];

type Props = {};

type State = {
    type: string,
    title: string,
    count: number
};

class WorkListPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            type: '',
            title: '',
            count: 0
        };
    }

    render(): React.ReactNode {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div style={{ maxHeight: "calc(100vh - 53px)", backgroundColor: "#875fc4", padding: 0, width: "250px" }}>
                        <div className="sticky-top sticky-margin" style={{ width: "250px", zIndex: 0, top: 0, bottom: 0, height: "calc(100vh - 53px)", overflow: 'hidden' }}>
                            <div style={{ padding: "20px 10px" }}>
                                <div style={{ color: "#feac00", fontSize: "25px", borderBottom: "2px solid #fdab00", paddingLeft: "10px", marginBottom: "20px" }}>
                                    รออนุมัติ
                                </div>
                                <div style={{ overflowY: 'auto', height: '78vh' }}>
                                    <div className="list-group">
                                        {
                                            menus.map((menu, index) => (
                                                <button key={index} className="list-group-item list-group-item-action" onClick={() => this.setState({ type: menu.type, title: menu.name, count: menu.count })}>
                                                    {menu.name} <span className="badge badge-accent-darker" style={{ backgroundColor: "#fe9200" }}>{menu.count}</span>
                                                </button>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <div className="sticky-margin">
                            {this.state.type === "" ?
                                <div style={{ paddingTop: "350px", textAlign: "center" }}>
                                    <div className="container">
                                        <h5>กรุณาเลือก menu</h5>
                                    </div>
                                </div>
                                :
                                <div style={{ paddingTop: "50px" }}>
                                    <div className="container">
                                        <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "20px" }}>
                                            <span>รออนุมัติ {this.state.title}</span>
                                        </h5>
                                    </div>
                                    <div className="container">
                                        <div className="card" style={{ backgroundColor: "#EDEDED", borderColor: "transparent", padding: "20px 20px 10px 20px", maxHeight: "500px", overflowY:"auto" }}>
                                            <table className="table table-bordered mb-3">
                                                <thead>
                                                    <tr>
                                                        <th>Refno</th>
                                                        <th>ชื่อลูกหนี้</th>
                                                        <th>เลขที่โฉนด</th>
                                                        <th>ที่ตั้ง</th>
                                                        <th>ต้นทุนทางบัญชี</th>
                                                        <th>View</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        [...Array(this.state.count)].map((i, index) => (
                                                            <tr key={index}>
                                                                <td>{index}</td>
                                                                <td>นาย {index}</td>
                                                                <td>โฉนดเลขที่ {index}</td>
                                                                <td>นครสวรรค์</td>
                                                                <td>{index * 100000}</td>
                                                                <td>
                                                                    <Link to="/activity"><i className="bi bi-file-earmark-text" style={{ color: "#7531c1", fontWeight: "500" }}></i></Link>
                                                                </td>
                                                            </tr>
                                                        )
                                                        )
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(WorkListPage);
