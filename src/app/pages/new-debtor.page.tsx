import React from 'react';
import { connect } from 'react-redux';
import NewDebtor from '../components/new-debtor';

type Props = {};

type State = {};

class NewDebtorPage extends React.Component<Props, State> {

    render(): React.ReactNode {
        return (
                <div className="container py-3 ">
                    <NewDebtor />
                </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(NewDebtorPage);