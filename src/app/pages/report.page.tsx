import React from 'react';
import { connect } from 'react-redux';
import { Link, Route, Routes } from "react-router-dom";
import ReportSellPropertyPage from "./reports/report-sell-property.page";
import ReportSellPropertyNPACoverPage from "./reports/report-sell-property-NPA-cover.page";
import ReportSellPropertyNPAPage from "./reports/report-sell-property-NPA.page";
import ReportSellPropertyInsurancePage from "./reports/report-sell-property-insurance.page";
import ReportSellPropertyTaxPage from "./reports/report-sell-property-tax.page";
import ReportCorporateTaxReturnPage from "./reports/report-corporate-tax-return.page";
import ReportReceiveTransferPage from "./reports/report-receive-transfer.page";
import ReportReceiveTransferNPAPage from "./reports/report-receive-transfer-NPA.page";
import ReportReceiveTransferInsurancePage from "./reports/report-receive-transfer-insurance.page";
import ReportConsolidateContractPage from "./reports/report-consolidate-contract.page";
import ReportDebtorLevelPage from "./reports/report-debtor-level.page";
import ReportPaymentTransferPage from "./reports/report-payment-transfer.page";
import ReportArrearPage from "./reports/report-arrear.page";
import ReportContractTransferMaturityPage from "./reports/report-contract-transfer-maturity.page";
import ReportRegisterExpulsionCostPage from "./reports/report-register-expulsion-cost.page";
import ReportTransferedAssetTypePage from "./reports/report-transfered-asset-type.page";
import ReportTenderOfferPage from "./reports/report-tender-offer.page";
import ReportTrackLackInstallmentPage from "./reports/report-track-lack-installment.page";
import ReportTrackPerformancePage from "./reports/report-track-performance.page";
import ReportRentPaymentPage from "./reports/report-rent-payment.page";
import ReportCommonFeePage from "./reports/report-common-fee.page";
import ReportPropertyPurchaseDutyPage from "./reports/report-property-purchase-duty.page";


export const menus = [
    {
        name: "[1] ขายทรัพย์",
        to: "/report/sell-property"
    },
    {
        name: "[2] ขายทรัพย์ส่งNPA-ใบปะหน้า",
        to: "/report/sell-property-NPA-cover"
    },
    {
        name: "[3] ขายทรัพย์ส่งNPA",
        to: "/report/sell-property-NPA"
    },
    {
        name: "[4] ขายทรัพย์ส่งทีมประกัน",
        to: "/report/sell-property-insurance"
    },
    {
        name: "[5] ขายทรัพย์ส่งทีมภาษี",
        to: "/report/sell-property-tax"
    },
    {
        name: "[6] สรุปรายการส่งภาษีนิติบุคคล",
        to: "/report/corporate-tax-return"
    },
    {
        name: "[7] รับโอนเพิ่ม",
        to: "/report/receive-transfer"
    },
    {
        name: "[8] รับโอนเพิ่มส่งNPA",
        to: "/report/receive-transfer-NPA"
    },
    {
        name: "[9] รับโอนเพิ่มส่งทีมประกัน",
        to: "/report/receive-transfer-insurance"
    },
    {
        name: "[10] ทำสัญญารวม",
        to: "/report/consolidate-contract"
    },
    {
        name: "[12] จัดชั้นลูกหนี้",
        to: "/report/debtor-level"
    },
    {
        name: "[13] รายงานทรัพย์สินโอนชำระหนี้",
        to: "/report/payment-transfer"
    },
    {
        name: "[14] รายงานทรัพย์สินรอรับโอนค้าง",
        to: "/report/arrear"
    },
    {
        name: "[15] รายการครบกำหนดโอนทรัพย์ทำสัญญา",
        to: "/report/contract-transfer-maturity"
    },
    // {
    //     name: "[16] ทะเบียนเบิกการใช้จ่ายในการฟ้องขับไล่",
    //     to: "/report/register-expulsion-cost"
    // },
    {
        name: "[17] รายงานทรัพย์สินโอนชำระหนี้ (แยกตามประเภท)",
        to: "/report/transfered-asset-type"
    },
    {
        name: "[19] รายการข้อมูลการเสนอซื้อทรัพย์",
        to: "/report/tender-offer"
    },
    {
        name: "[20] รายงานติดตามการขาดผ่อนชำระ",
        to: "/report/track-lack-installment"
    },
    // {
    //     name: "[38] รายการติดตามผลการดำเนินงานทั้งหมด",
    //     to: "/report/track-performance"
    // },
    {
        name: "[39] รับชำระค่าเช่า",
        to: "/report/rent-payment"
    },
    {
        name: "[40] ค่าส่วนกลาง",
        to: "/report/common-fee"
    },
    {
        name: "[41] ค่าอารกรฯ ซื้อทรัพย์",
        to: "/report/property-purchase-duty"
    }

];

type Props = {};

type State = {
    name: string
};

class ReportPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            name: ''
        };
    }

    render(): React.ReactNode {
        return (

            <div className="container-fluid">

                <div className="row">
                    <div style={{ maxHeight: "calc(100vh - 53px)", backgroundColor: "#875fc4", padding: 0, width: "250px" }}>
                        <div className="sticky-top sticky-margin" style={{ width: "250px", zIndex: 0, top: 0, bottom: 0, height: 'calc(100vh - 53px)', overflow: 'hidden' }}>
                            <div style={{ padding: "20px 10px" }}>
                                <div style={{ color: "#feac00", fontSize: "25px", borderBottom: "2px solid #fdab00", paddingLeft: "10px", marginBottom: "20px" }}>
                                    REPORTS
                                </div>
                                <div style={{ overflowY: 'auto', height: '78vh' }}>
                                    <div className="list-group">
                                        {
                                            menus.map((menu, index) => (
                                                <Link key={index} to={menu.to ? menu.to : "/"} className="list-group-item list-group-item-action" onClick={() => this.setState({ name: menu.name})}>
                                                    {menu.name} 
                                                </Link>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div className="col">
                        <div className="sticky-margin">
                            {this.state.name === "" ?
                                <div style={{ paddingTop: "300px", textAlign: "center" }}>
                                    <div className="container">
                                        <h5>กรุณาเลือก menu</h5>
                                    </div>
                                </div>
                                :
                                <Routes>
                                    <Route path="/sell-property" element={<ReportSellPropertyPage />} />
                                    <Route path="/sell-property-NPA-cover" element={<ReportSellPropertyNPACoverPage />} />
                                    <Route path="/sell-property-NPA" element={<ReportSellPropertyNPAPage />} />
                                    <Route path="/sell-property-insurance" element={<ReportSellPropertyInsurancePage />} />
                                    <Route path="/sell-property-tax" element={<ReportSellPropertyTaxPage />} />
                                    <Route path="/corporate-tax-return" element={<ReportCorporateTaxReturnPage />} />
                                    <Route path="/receive-transfer" element={<ReportReceiveTransferPage />} />
                                    <Route path="/receive-transfer-NPA" element={<ReportReceiveTransferNPAPage />} />
                                    <Route path="/receive-transfer-insurance" element={<ReportReceiveTransferInsurancePage />} />
                                    <Route path="/consolidate-contract" element={<ReportConsolidateContractPage />} />
                                    <Route path="/debtor-level" element={<ReportDebtorLevelPage />} />
                                    <Route path="/payment-transfer" element={<ReportPaymentTransferPage />} />
                                    <Route path="/arrear" element={<ReportArrearPage />} />
                                    <Route path="/contract-transfer-maturity" element={<ReportContractTransferMaturityPage />} />
                                    <Route path="/register-expulsion-cost" element={<ReportRegisterExpulsionCostPage />} />
                                    <Route path="/transfered-asset-type" element={<ReportTransferedAssetTypePage />} />
                                    <Route path="/tender-offer" element={<ReportTenderOfferPage />} />
                                    <Route path="/track-lack-installment" element={<ReportTrackLackInstallmentPage />} />
                                    <Route path="/track-performance" element={<ReportTrackPerformancePage />} />
                                    <Route path="/rent-payment" element={<ReportRentPaymentPage />} />
                                    <Route path="/common-fee" element={<ReportCommonFeePage />} />
                                    <Route path="/property-purchase-duty" element={<ReportPropertyPurchaseDutyPage />} />
                                </Routes>
                            }
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ReportPage);