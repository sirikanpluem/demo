import React from 'react';
import { connect } from 'react-redux';
import { authenticationAsync } from '../reducers/authorization.reducer';

type Props = {} & any;

type State = {
    username: string,
    password: string
};
class LoginPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            username: '',
            password: ''
        };
    }

    onSubmit = (username: string, password: string) => (ev: React.SyntheticEvent) => {
        ev.preventDefault();
        this.props.authenticationAsync(username, password);
    };


    render(): React.ReactNode {

        const { username, password } = this.state;
        const valid = !!(username && password);

        return (
            <div className='p-3' style={{ width: '400px', height: '370px', top: '50%', left: '50%', margin: '-185px 0 0 -200px', position: 'absolute' }}>
                <div className='mb-3'>
                </div>
                <form style={{ maxWidth: '320px', margin: '0 auto' }} onSubmit={this.onSubmit(username, password)}>
                    <div className='form-floating mb-3'>
                        <input type='text' id='username' className='form-control' placeholder='Username' autoComplete='off' onChange={
                            (e) => this.setState({ username: e.target.value })
                        } />
                        <label htmlFor='username'>Username</label>
                    </div>
                    <div className='form-floating mb-3'>
                        <input type='password' id='password' className='form-control' placeholder='Password' onChange={
                            (e) => this.setState({ password: e.target.value })
                        } />
                        <label htmlFor='password'>Password</label>
                    </div>
                    <div className='row'>
                        <div className='col-6'>
                            <button type='submit' className='btn btn-primary w-100' disabled={!valid}>Login</button>
                        </div>
                        <div className='col-6'>
                            <button type='reset' className='btn btn-secondary w-100'>Clear</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({
    authenticationAsync: (username: string, password: string) =>
        dispatch(authenticationAsync({ username, password }))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);