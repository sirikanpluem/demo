import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";

type Props = {};

type State = {
    type: 'DEBTOR' | 'PROPERTY',
    data: { refNo: string, debtor: string, doc: string, address: string, principle: string }[]
};

class SearchPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            type: 'DEBTOR',
            data: []
        };
    }

    render(): React.ReactNode {
        return (
            <div className="container" style={{ paddingTop: "50px" }}>
                <h5 style={{ borderBottom: "2px solid", paddingBottom: "10px", marginBottom: "50px" }}>Search</h5>
                <div className="row">
                    <div className="col-12 col-sm-5 col-md-4">
                        <div className="card" style={{ backgroundColor: "#F6EFFF", borderColor: "transparent" }}>
                            <form>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-6">
                                            <div className="form-check form-check-inline mb-3">
                                                <input className="form-check-input" type="radio" name="type" id="debtor" value="DEBTOR" onChange={(e) => this.setState({
                                                    type: "DEBTOR"
                                                })} defaultChecked />
                                                <label className="form-check-label" htmlFor="debtor">
                                                    ค้นหาลูกหนี้
                                                </label>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="form-check form-check-inline mb-3">
                                                <input className="form-check-input" type="radio" name="type" id="property" value="PROPERTY" onChange={(e) => this.setState({
                                                    type: "PROPERTY"
                                                })} />
                                                <label className="form-check-label" htmlFor="property">
                                                    ค้นหาจากทรัพย์
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {this.state.type === "DEBTOR" ?
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="mb-3 row">
                                                    <label htmlFor="staticEmail" className="col-sm-5 col-form-label text-end">Reference Number</label>
                                                    <div className="col-sm-7">
                                                        <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-12">
                                                <div className="mb-3 row">
                                                    <label htmlFor="staticEmail" className="col-sm-5 col-form-label text-end">ชื่อบัญชี</label>
                                                    <div className="col-sm-7">
                                                        <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        :
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="mb-3 row">
                                                    <label htmlFor="staticEmail" className="col-sm-5 col-form-label text-end">รหัสทรัพย์</label>
                                                    <div className="col-sm-7">
                                                        <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-12">
                                                <div className="mb-3 row">
                                                    <label htmlFor="staticEmail" className="col-sm-5 col-form-label text-end">เลขที่เอกสารสิทธิ์</label>
                                                    <div className="col-sm-7">
                                                        <input type="text" className="form-control" id="staticEmail" autoComplete="off" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="row" style={{ width: "100%", maxWidth: "320px", float: "right" }}>
                                                <div className="col-6">
                                                    <button type="button" className="btn btn-primary w-100" onClick={() => this.setState({ data: [{ refNo: "a", debtor: "a", doc: "a", address: "a", principle: "a" }] })}>Search</button>
                                                </div>
                                                <div className="col-6">
                                                    <button type="reset" className="btn btn-secondary w-100" onClick={() => this.setState({ data: [] })}>Clear</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="col-12 col-sm-7 col-md-8">
                        {this.state.data.length !== 0 ?
                            <div className="container">
                                <div className="card" style={{ backgroundColor: "#EDEDED", borderColor: "transparent", padding: "20px 20px 10px 20px" }}>
                                    <table className="table table-bordered mb-3">
                                        <thead>
                                            <tr>
                                                <th>Refno</th>
                                                <th>ชื่อลูกหนี้</th>
                                                <th>เลขที่โฉนด</th>
                                                <th>ที่ตั้ง</th>
                                                <th>ต้นทุนทางบัญชี</th>
                                                <th>View</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>304918</td>
                                                <td>นาย ก ข</td>
                                                <td>โฉนดเลขที่ 71140</td>
                                                <td>นครสวรรค์</td>
                                                <td>1,822,273.03</td>
                                                <td>
                                                    <Link to="/activity"><i className="bi bi-file-earmark-text"></i></Link>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>9902383</td>
                                                <td>นาย เอ</td>
                                                <td>โฉนดเลขที่ 179345, 172229</td>
                                                <td>กรุงเทพ</td>
                                                <td>2,691,157.35</td>
                                                <td>
                                                    <Link to="/activity"><i className="bi bi-file-earmark-text"></i></Link>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            : null}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({ ...state });

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
