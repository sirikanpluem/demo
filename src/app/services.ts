import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:8000/',
    headers: {
        'Content-Type': 'application/json'
    }
});

instance.interceptors.request.use((config: any) => {
    config.headers['Authorization'] = 'Bearer ok';
    return config;
});

const AuthService = {
    ldap: (data: { username: string, password: string }) => instance.post('/amisapi/authentication/ldap', data)
};

export {
    AuthService
};