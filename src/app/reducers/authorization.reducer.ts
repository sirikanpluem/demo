import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { Navigate } from 'react-router';
import { RootState } from '../../redux/store';
import { AuthService } from '../services';

type User = {
    username: string;
    roleName: string;
};

export interface AuthorizationState {
    user: undefined | User,
    status: 'idle' | 'loading' | 'failed';
}

const initialState: AuthorizationState = {
    user: undefined,
    status: 'idle'
};

export const authenticationAsync = createAsyncThunk(
    'authentication/ldap',
    async (payload: { username: string, password: string }) => {
        const resp = await AuthService.ldap(payload);
        return resp.data;
    }
);

export const authorizationSlice = createSlice({
    name: 'authorization',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder
            .addCase(authenticationAsync.pending, state => {
                state.status = 'loading';
            })
            .addCase(authenticationAsync.fulfilled, (state, action) => {
                state.status = 'idle';
                const { username, roleName } = action.payload;

                state.user = {
                    username,
                    roleName
                };
            })
            .addCase(authenticationAsync.rejected, state => {
                state.status = 'failed';
            })
    }
});

export const selectAuthorization = (state: RootState) => state.authorization.user;

export default authorizationSlice.reducer;